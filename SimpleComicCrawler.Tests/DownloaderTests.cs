﻿namespace SimpleComicCrawler.Tests
{
    using System.Drawing;
    using System.Drawing.Imaging;

    using NUnit.Framework;

    public class DownloaderTests
    {
        private static CustomDownloader customDownloader;

        [SetUp]
        public void SetUp()
        {
            customDownloader = new CustomDownloader();
        }

        [Test]
        public void CanDownloadFirstImage()
        {
            // Act
            var image = customDownloader.DownloadNextImage();

            // Assert
            Assert.That(image.Size, Is.GreaterThan(0));
        }

        [Test]
        public void DownloadedImageIsJpeg()
        {
            // Act
            var image = customDownloader.DownloadNextImage();

            // Assert
            Assert.That(image.Bitmap.RawFormat, Is.EqualTo(ImageFormat.Jpeg));
        }

        [Test]
        public void ImageIsSavedToDisk()
        {
            // Act
            var image = customDownloader.DownloadNextImage();
            Bitmap bitmap = new Bitmap(image.FilePath);

            // Assert
            Assert.That(bitmap.RawFormat, Is.EqualTo(ImageFormat.Jpeg));
        }

        [Test]
        public void CanDownloadNextImage()
        {
            // Act
            customDownloader.DownloadNextImage();
            var nextImage = customDownloader.DownloadNextImage();

            // Assert
            Assert.That(nextImage.Bitmap.RawFormat, Is.EqualTo(ImageFormat.Jpeg));
        }

        [Test]
        public void NextImageIsDifferentFromPrevious()
        {
            // Act
            var firstImage = customDownloader.DownloadNextImage();
            var nextImage = customDownloader.DownloadNextImage();

            // Assert
            Assert.That(nextImage.Size, Is.Not.EqualTo(firstImage.Size));
        }
    }
}
