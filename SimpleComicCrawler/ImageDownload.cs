namespace SimpleComicCrawler
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Linq;

    public class ImageDownload
    {
        public Bitmap Bitmap { get; }

        private string Name { get; }

        public string FilePath { get; }

        public long Size { get; private set; }

        private static string GetFilenameExtension(ImageFormat format)
        {
            return ImageCodecInfo.GetImageEncoders()
                .FirstOrDefault(x => x.FormatID == format.Guid)
                ?.FilenameExtension.Split(';').First().Substring(2);
        }

        public ImageDownload(Bitmap bitmap, long size, string name)
        {
            Bitmap = bitmap;
            Size = size;
            Name = name;

            FilePath = string.Format(
                "{0}.{1}", Name, GetFilenameExtension(bitmap.RawFormat));

            if (FilePath != null)
            {
                bitmap.Save(FilePath);
            }

            Console.WriteLine(FilePath);
        }
    }
}