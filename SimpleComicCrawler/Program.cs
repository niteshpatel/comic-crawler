﻿namespace SimpleComicCrawler
{
    static class Program
    {
        static void Main()
        {
            var downloader = new CustomDownloader();

            downloader.DownloadAllImages();
        }
    }
}
