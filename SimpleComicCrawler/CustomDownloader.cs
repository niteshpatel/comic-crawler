﻿namespace SimpleComicCrawler
{
    using CsQuery;

    public class CustomDownloader : Downloader
    {
        const string StartUrl = "http://fill-me-in.test";

        public CustomDownloader()
            : base(StartUrl)
        {
        }

        protected override string GetNextPageUrl(CQ dom)
        {
            // TODO: this needs to return the URL of the next page
            return string.Empty;
        }

        protected override string GetImageUrl(CQ dom)
        {
            // TODO: this needs to return the SRC of image
            return string.Empty;
        }
    }
}
