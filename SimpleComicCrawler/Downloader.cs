namespace SimpleComicCrawler
{
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;

    using CsQuery;

    public abstract class Downloader
    {
        protected Downloader(string startUrl)
        {
            Counter = 0;
            NextPageUrl = startUrl;
        }

        private int Counter { get; set; }

        private string NextPageUrl { get; set; }

        public ImageDownload DownloadNextImage()
        {
            string imageName = string.Format(
                "{0}-{1}", Counter.ToString("D3"), NextPageUrl.Split('/').Last());

            CQ pageDom = GetPageDom();

            NextPageUrl = GetNextPageUrl(pageDom);
            Counter ++;

            string imageUrl = GetImageUrl(pageDom);

            var imageBytes = GetImageBytes(imageUrl);

            var image = new ImageDownload(
                new Bitmap(new MemoryStream(imageBytes)), imageBytes.Length, imageName);

            return image;
        }

        public void DownloadAllImages()
        {
            while (NextPageUrl != null)
            {
                DownloadNextImage();
            }
        }

        protected abstract string GetImageUrl(CQ pageDom);

        protected abstract string GetNextPageUrl(CQ pageDom);

        private byte[] GetImageBytes(string url)
        {
            return new WebClient().DownloadData(url);
        }

        private CQ GetPageDom()
        {
            return CQ.CreateFromUrl(NextPageUrl);
        }
    }
}